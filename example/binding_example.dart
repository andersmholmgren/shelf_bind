// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_route/shelf_route.dart';
import 'package:shelf_bind/shelf_bind.dart';
import 'package:http_exception/http_exception.dart';
import 'package:shelf_exception_handler/shelf_exception_handler.dart';
import 'dart:async';
import 'package:constrain/constrain.dart';
import 'package:matcher/matcher.dart';

void main() {
  var customObjects = {
    // note we may need to get authentication info from the request
    // and based on that lookup a user from the db before creating an
    // authenticated client. Simulating here by returning a future
    PersonLookupClient: (req) => new Future.value(new PersonLookupClient())
  };

  var myRouter = router(
          handlerAdapter: handlerAdapter(
              customObjects: customObjects,
              validateParameters: true,
              validateReturn: true))
      ..get('/', () => "Hello World")
      ..get('/later', () => new Future.value("Hello World"))
      ..get('/map', () => {"greeting": "Hello World"})
      ..get('/object', () => new SayHello()..greeting = "Hello World")
      ..get('/ohnoes', () => throw new BadRequestException())
      ..get('/response', () => new Response.ok("Hello World"))
      ..get('/greeting/{name}', (String name) => "Hello $name")
      ..get('/greeting2/{name}{?age}',
          (String name, int age) => "Hello $name of age $age")
      ..get('/greeting3/{name}', (Person person) => "Hello ${person.name}")
      ..get(
          '/greeting4/{name}', (MutablePerson person) => "Hello ${person.name}")
      ..get(
          '/greeting5/{name}',
          (String name, Request request) => "Hello $name ${request.method}")
      ..post('/greeting5', _create)
      ..post('/greeting6', (Person person) => "Hello ${person.name}")
      ..get(
          '/greeting7/{name}',
          (String name, PersonLookupClient client) => client.lookup(name))
      ..get('/greeting8{?name}',
          (@PathParams() Person person) => "Hello ${person.name}");

  printRoutes(myRouter);

  var handler = const Pipeline()
      .addMiddleware(logRequests())
      .addMiddleware(exceptionHandler())
      .addHandler(myRouter.handler);

  io.serve(handler, 'localhost', 8080).then((server) {
    print('Serving at http://${server.address.host}:${server.port}');
  });
}

// TODO: kinda a dumb example
@ResponseHeaders.created(idField: #name)
Person _create(@RequestBody() Person person) => person;

class SayHello {
  String greeting;

  Map toJson() => {'greeting': greeting};
}

class Person {
  @NotNull()
  @Ensure(nameIsAtLeast3Chars,
      description: 'name must be at least 3 characters')
  final String name;

  @NotNull()
  @Ensure(isNotEmpty)
  @Ensure(allStreetsStartWith15, description: "all streets must start with 15")
  List<Address> addresses;

  Person.build({this.name});

  Person.fromJson(Map json)
      : this.name = json['name'],
        this.addresses = _addressesFromJson(json['addresses']);

  static List<Address> _addressesFromJson(json) {
    if (json == null || json is! List) {
      return null;
    }

    return json.map((a) => new Address.fromJson(a)).toList(growable: false);
  }

  Map toJson() => {'name': name, 'addresses': addresses};

  String toString() => 'Person[name: $name]';
}

class Address {
  @Ensure(streetIsAtLeast10Characters)
  String street;

  Address.fromJson(Map json) : this.street = json['street'];

  Map toJson() => {'street': street};

  String toString() => 'Address[street: $street]';
}

Matcher nameIsAtLeast3Chars() => hasLength(greaterThan(3));

bool allStreetsStartWith15(List<Address> addresses) =>
    addresses.every((a) => a.street == null || a.street.startsWith("15"));

Matcher streetIsAtLeast10Characters() => hasLength(greaterThanOrEqualTo(10));

class MutablePerson {
  String name;
}

class PersonLookupClient {
  Future<Person> lookup(String name) =>
      new Future.value(new Person.build(name: name));
}
