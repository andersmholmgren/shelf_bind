// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.adapter.handler;

import 'package:shelf/shelf.dart' show Handler, Request, Response;
import 'package:path/path.dart' as p show url;

import 'parameter_bindings.dart';

import 'package:shelf_route/shelf_route.dart';
import 'package:shelf_bind/src/util/preconditions.dart';
import 'package:matcher/matcher.dart';
import 'package:shelf_bind/src/model/handler_bindings.dart';
import 'package:shelf_bind/src/inferencing/handler_bindings_inferencer.dart';

final _url = p.url;

/// a [HandlerAdapter] for shelf_bind.
///
/// It implements [MergeableHandlerAdapter] such that [customObjects]
/// are merged
class BoundHandlerAdapter implements MergeableHandlerAdapter {
  final Map<Type, CustomObjectFactory> customObjects;
  final bool validateParameters;
  final bool validateReturn;

  BoundHandlerAdapter(
      {this.customObjects: const {},
      bool validateParameters: false,
      bool validateReturn: false})
      : this.validateParameters = _default(validateParameters, false),
        this.validateReturn = _default(validateReturn, false) {
    ensure(customObjects, isNotNull);
    ensure(this.validateParameters, isNotNull);
    ensure(this.validateReturn, isNotNull);
  }

  BoundHandlerAdapter merge(BoundHandlerAdapter child) {
    return new BoundHandlerAdapter(
        validateParameters: validateParameters,
        validateReturn: validateReturn,
        customObjects: {}..addAll(child.customObjects)..addAll(customObjects));
  }

  Handler call(Function handler) => handlerAdapter(handler);

  HandlerAdapter get handlerAdapter =>
      (Function handler) => handlerBinding(handler).handler;

  HandlerBinding handlerBinding(Function handler) => createHandlerBinding(
      handler, customObjects, validateParameters, validateReturn);
}

_default(value, defaultValue) => value != null ? value : defaultValue;
