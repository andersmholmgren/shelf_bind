// Copyright (c) 2015, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.handler;

import 'package:shelf/shelf.dart' show Handler, Request, Response;
import 'dart:mirrors' show ClosureMirror, reflect;
import 'dart:async' show Future;
import 'package:path/path.dart' as p show url;
import 'validation.dart';

import 'parameter_bindings.dart';
import 'response_bindings.dart';

import 'package:http_exception/http_exception.dart'
    show BadRequestException, HttpException;
import 'dart:io' show HttpStatus;
import 'package:option/option.dart';
import 'package:shelf_bind/src/util/preconditions.dart';
import 'package:shelf_bind/src/util/mirror_util.dart';
import 'package:shelf_bind/src/util/util.dart';
import 'package:matcher/matcher.dart';

final _url = p.url;

/// A binding between an Dart [Function] and a [shelf] [Handler]
///
/// Contains the bindings to all the function parameters in [parameterBindings]
/// and to the response in [responseBinding]
///
/// The [Handler] can be obtained via the [handler] property
class HandlerBinding {
  final List<HandlerParameterBinding> parameterBindings;
  final _BoundHandler _boundHandler;
  final ResponseBinding responseBinding;
  final ClosureMirror _handlerMirror;

  // TODO: avoid reflecting again
  HandlerBinding(this.parameterBindings, Function handler,
      [ResponseBinding responseBinding,
      bool validateParameters = true,
      bool validateReturn = false])
      : this._boundHandler =
            _wrapHandler(handler, validateParameters, validateReturn),
        this.responseBinding = responseBinding != null
            ? responseBinding
            : new JsonResponseBinding.ok(),
        this._handlerMirror = reflect(handler) {
    ensure(parameterBindings, isNotNull);
    ensure(_boundHandler, isNotNull);
    ensure(_handlerMirror, isNotNull);
    parameterBindings.forEach((pb) => pb.validate());
  }

  Handler get handler => _handleRequest;

  dynamic _handleRequest(Request request) {
    Future create(HandlerParameterBinding pb) =>
        new Future.sync(() => pb.createInstance(request));

    final positionalParameterFutureValues =
        parameterBindings.where((pb) => !pb.isNamed).map(create);

    final Map<String, HandlerParameterBinding> namedParameters =
        new Map<String, HandlerParameterBinding>.fromIterable(
            parameterBindings.where((pb) => pb.isNamed),
            key: (HandlerParameterBinding v) => v.handlerParameterName);

    final Map<Symbol, Future> namedParameterFutureValues = <Symbol, Future>{};
    namedParameters.forEach((String n, pb) {
      namedParameterFutureValues[new Symbol(n)] = create(pb);
    });

//    .map((pb) async => await pb.createInstance(request));

    return _boundHandler(positionalParameterFutureValues,
        namedParameterFutureValues, request, responseBinding);
  }

  Option getHandlerAnnotation(Type baseType) =>
      getAnnotationOpt(_handlerMirror.function, baseType);
}

typedef _BoundHandler(
    Iterable<Future> positionalArguments,
    Map<Symbol, Future> namedArguments,
    Request request,
    ResponseBinding responseBinding);

_BoundHandler _wrapHandler(
    Function handler, bool validateParameters, bool validateReturn) {
  final ClosureMirror handlerMirror = reflect(handler);
  final functionMirror = handlerMirror.function;

  return (Iterable<Future> positionalArguments,
      Map<Symbol, Future> namedArguments,
      Request request,
      ResponseBinding responseBinding) async {
    try {
      final args = await Future.wait([
        Future.wait(positionalArguments),
        Future.wait(namedArguments.values)
      ]);

      final pa = args[0];
      final namedArgValues = args[1];

      final Map<Symbol, dynamic> namedArgs = {};
      final keys = namedArguments.keys;
      for (int i = 0; i < keys.length; i++) {
        final value = namedArgValues[i];
        if (value != null) {
          namedArgs[keys.elementAt(i)] = namedArgValues[i];
        }
      }

      final response = await constrainedFunctionProxy.invoke(handlerMirror, pa,
          namedArguments: namedArgs,
          validateParameters: validateParameters,
          validateReturn: validateReturn);

      if (response == null) {
        return new Response.internalServerError(
            body: 'function ${functionMirror.simpleName} returned null');
      }
      return responseBinding.createResponse(request, reflect(response));
    } on ParameterConstraintViolationException catch (e) {
      throw new BadRequestException({'errors': iterableToJson(e.violations)});
    } on ArgumentError catch (e, stackTrace) {
      print(e);
      print(stackTrace);
      throw new BadRequestException({'errors': e.toString()}, e.toString());
    } on FormatException catch (e) {
      throw new BadRequestException({
        'errors':
            'Failed to convert request parameters. Invalid value ${e.message}'
      });
    } on ReturnConstraintViolationException catch (e) {
      throw new HttpException(HttpStatus.INTERNAL_SERVER_ERROR,
          "Internal Server Error", {'errors': iterableToJson(e.violations)});
    }
  };
}
