// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.annotation;

import 'dart:io' show HttpStatus;
import 'package:option/option.dart';

abstract class RequestHandlerParam {
  final Type type;

  const RequestHandlerParam(this.type);
}

abstract class ComplexRequestHandlerParam extends RequestHandlerParam {
  final BindMode bindMode;

  const ComplexRequestHandlerParam(Type type, this.bindMode) : super(type);
}

/**
 * An annotation that indicates that a handler parameter should be bound to the request
 * body. Note that this annotation can often be omitted, as the default
 * behaviour is for non basic types to be mapped to a JSON body.
 * You should provide this annotation when you want to override this behaviour.
 *
 * The body will be parsed into an object of the specified [type].
 * Note that [type] is typically omitted, in which case it is determined via
 * mirrors
 *
 * The default format for the body is JSON.
 *
 * [bindMode] can be provided to control how the binding to class fields occurs
 * when [format] is [ContentType.FORM]. It is ignored for [ContentType.JSON].
 * By default it will attempt to bind through constructor fields and then
 * via setters.
 */
class RequestBody extends ComplexRequestHandlerParam {
  final ContentType format;

  const RequestBody(
      {this.format: ContentType.JSON,
      Type type,
      BindMode bindMode: BindMode.CONSTRUCTOR_THEN_PROPERTY})
      : super(type, bindMode);
}

/**
 * An annotation that indicates that a handler parameter should be bound to
 * a single request parameter.
 *
 * This annotation can typically be omitted as the default
 * behaviour is to bind simple type handler parameters to a single path parameter
 * of the same name.
 *
 * Note that [type] is typically omitted, in which case it is determined via
 * mirrors
 *
 * You need to provide this annotation if:
 *   - the path parameter has a different name to the path parameter
 *   - the type to convert to is different from that which would be determined
 *   via mirrors
 */
class PathParam extends RequestHandlerParam {
  /// The name of the path parameter
  final String pathName;

  const PathParam({this.pathName, Type type}) : super(type);
}

class HeaderParam extends RequestHandlerParam {
  final String headerName;

  const HeaderParam({this.headerName, Type type}) : super(type);
}

/**
 * An annotation that indicates that a complex handler parameter should be bound
 * to one or more request parameters.
 *
 * Note that [type] is typically omitted, in which case it is determined via
 * mirrors
 *
 * [bindMode] can be provided to control how the binding to class fields occurs..
 * By default it will attempt to bind through constructor fields and then
 * via setters.
 *
 * If [bindMode] is [BindMode.CONSTRUCTOR] or
 * [BindMode.CONSTRUCTOR_THEN_PROPERTY] then [constructorName] can be used to
 * override the default name of #build.
 *
 * [memberBindings] can be specified for any class property names that don't
 * match the corresponding path parameter names
 */
class PathParams extends ComplexRequestHandlerParam {
  final Symbol constructorName;
  final Map<String, Symbol> memberBindings;

  const PathParams(
      {Type type,
      BindMode bindMode: BindMode.CONSTRUCTOR_THEN_PROPERTY,
      this.constructorName: #build,
      this.memberBindings: const {}})
      : super(type, bindMode);
}

/// Describes how the data (e. path parameters, form data) will be bound to
/// the class.
///
/// It can either be done via a constructor with named optional arguments,
/// or via setters.
///
/// [CONSTRUCTOR_THEN_PROPERTY] means that it will use a constructor if one
/// is found with the correct name or else fallback to setters.
///
class BindMode {
  final String literal;

  const BindMode._internal(this.literal);

  static const BindMode CONSTRUCTOR = const BindMode._internal("CONSTRUCTOR");
  static const BindMode PROPERTY = const BindMode._internal("PROPERTY");
  static const BindMode CONSTRUCTOR_THEN_PROPERTY =
      const BindMode._internal("CONSTRUCTOR_THEN_PROPERTY");

  String toString() => literal;
}

/**
 * Annotation to tweak default creating of responses
 */
class ResponseHeaders {
  /// The HttpStatus to use if the handler returns normally (no exception thrown).
  /// Default is OK. Most common use would be to return CREATED instead for
  /// certain operations
  final int successStatus;

  /// Set to [true] if the location header should be populated automatically.
  /// Default is false.
  /// If [true] the location header is populated to the incoming request url,
  /// which is normally correct if creating via a PUT (but not POST)
  final bool setLocation;

  /// The name of the field that is the primary key for the class.
  /// This will be used when setting the location header.
  /// Default is `id`
  final Symbol idField;

  const ResponseHeaders(
      {this.successStatus: HttpStatus.OK,
      this.setLocation: false,
      this.idField: #id});

  /// Return a CREATED status code and populate the location automatically
  const ResponseHeaders.created({Symbol idField: #id})
      : this(
            successStatus: HttpStatus.CREATED,
            setLocation: true,
            idField: idField);
}

/**
 * Representation of a content type. An instance of [ContentType] is
 * immutable.
 */
class ContentType {
  /**
   * Gets the mime-type, without any parameters.
   */
  String get mimeType => '$primaryType/$subType';

  /**
   * Gets the primary type.
   */
  final String primaryType;

  /**
   * Gets the sub type.
   */
  final String subType;

  /**
   * Gets the character set.
   */
  final String charset;

  /**
   * Content type for plain text using UTF-8 encoding.
   *
   *     text/plain; charset=utf-8
   */
  static const TEXT =
      const ContentType._internal("text", "plain", charset: "utf-8");

  /**
   *  Content type for HTML using UTF-8 encoding.
   *
   *     text/html; charset=utf-8
   */
  static const HTML =
      const ContentType._internal("text", "html", charset: "utf-8");

  /**
   *  Content type for JSON using UTF-8 encoding.
   *
   *     application/json; charset=utf-8
   */
  static const JSON =
      const ContentType._internal("application", "json", charset: "utf-8");

  /**
   *  Content type for binary data.
   *
   *     application/octet-stream
   */
  static const BINARY =
      const ContentType._internal("application", "octet-stream");

  /**
   *  Content type for form data using UTF-8 encoding.
   *
   *     application/x-www-form-urlencoded; charset=utf-8
   */
  static const FORM = const ContentType._internal(
      "application", "x-www-form-urlencoded",
      charset: "utf-8");

  /**
   *  Content type for multipart form data.
   *
   *     multipart/form-data
   */
  static const MULTIPART =
      const ContentType._internal("multipart", "form-data");

  const ContentType._internal(this.primaryType, this.subType, {this.charset});

  static Iterable<ContentType> get all =>
      [TEXT, HTML, JSON, BINARY, FORM, MULTIPART];

  static Option<ContentType> lookup(String mimeType) {
//    if (mimeType == null) {
//      return const None();
//    }
//    final mediaType = new MediaType.parse(mimeType);
    return new Option(
        all.firstWhere((ct) => ct.mimeType == mimeType, orElse: () => null));
  }

  String toString() => mimeType;
}
