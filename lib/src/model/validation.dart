// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.validation;

import 'package:constrain/constrain.dart';
import 'package:constrain/constrained_function_proxy.dart';
export 'package:constrain/constrain.dart'
    show ConstraintViolationException, ConstraintViolation;
export 'package:constrain/constrained_function_proxy.dart'
    show
        ParameterConstraintViolationException,
        ReturnConstraintViolationException;

final Validator _validator = new Validator();

final ConstrainedFunctionProxy constrainedFunctionProxy =
    new ConstrainedFunctionProxy(_validator);
