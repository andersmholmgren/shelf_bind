// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.core;

import 'package:shelf_bind/src/model/annotations.dart';
import 'package:shelf_bind/src/util/preconditions.dart';
import 'package:matcher/matcher.dart';
import 'package:shelf_bind/src/model/parameter_bindings.dart';

class TypeBinding {
  final Type type;
  final Symbol constructorName;
  final Iterable<MemberBinding> memberBindings;
  final BindMode bindMode;

  TypeBinding.def(
      this.type, this.constructorName, this.bindMode, this.memberBindings) {
//    _validateBindings(); TODO: don't like turning this off
  }

  TypeBinding.build(
      {this.type, this.constructorName, this.bindMode, this.memberBindings});

  TypeBinding(Type type,
      {Symbol constructorName: #build,
      BindMode bindMode: BindMode.CONSTRUCTOR_THEN_PROPERTY,
      Map<String, MemberBinding> memberBindings: const {}})
      : this.def(
            type, constructorName, bindMode, _convertBindings(memberBindings));

  TypeBinding.create(Type type,
      {Symbol constructorName: #build,
      BindMode bindMode: BindMode.CONSTRUCTOR_THEN_PROPERTY,
      Map<String, Symbol> memberBindings: const {}})
      : this.def(
            type, constructorName, bindMode, _createBindings(memberBindings));

  TypeBinding.fromAnnotation(PathParams annotation)
      : this.def(annotation.type, annotation.constructorName,
            annotation.bindMode, _createBindings(annotation.memberBindings));

  static Iterable<MemberBinding> _createBindings(
      Map<String, Symbol> memberBindings) {
    return memberBindings.keys.map((n) {
      final memberName = memberBindings[n];
      return new MemberBinding(new Symbol(n), memberName, null);
    });
  }

  static Iterable<MemberBinding> _convertBindings(
      Map<String, MemberBinding> memberBindings) {
    if (memberBindings == null) {
      return const [];
    }

    return memberBindings.keys.map((n) {
      final b = memberBindings[n];
      return new MemberBinding(new Symbol(n), b.memberName, b.converter);
    });
  }

  validateBindings() {
    ensure(type, isNotNull);
    ensure(constructorName, isNotNull);
    ensure(memberBindings, isNotNull);
    ensure(memberBindings, isNot(isEmpty));
    ensure(bindMode, isNotNull);
  }
}

class MemberBinding {
  final Symbol pathParameterName;
  final Symbol memberName;
  final ParameterTypeConverter converter;

  MemberBinding(this.pathParameterName, this.memberName, this.converter);

  convert(value) => converter != null ? converter.convert(value) : value;
}
