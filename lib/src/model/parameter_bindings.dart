// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.param;

import 'package:shelf/shelf.dart' show Request;
import 'package:shelf_path/shelf_path.dart'
    show getPathParameter, getPathParameters;

import 'dart:mirrors';

import 'type_bindings.dart';
import 'dart:convert';
import 'package:shelf_bind/src/util/preconditions.dart';
import 'package:matcher/matcher.dart';
import 'package:shelf_bind/src/model/annotations.dart';
import 'package:shelf_bind/src/util/case_converters.dart';
import 'package:converters/converters.dart';

/**
 * Describes how a single parameter of a handler function is populated.
 * It includes (either explicitly or implicitly) where the data comes from
 * (almost always from the [Request] or derived from the request) and any
 * transformations that are applied to that data.
 *
 * Different sorts of bindings are supported such as:
 * * the [request] object itself (see [RequestObjectParameterBinding])
 * * user provided objects (see [CustomObjectParameterBinding])
 * * path parameters to simple type (see [BasicHandlerParameterBinding] and
 * [PathHandlerParameterSource])
 * * path parameters to class types (see [TypePathParameterBinding] and
 * [MultiPathHandlerParameterSource])
 * * request body to class types (see [BasicHandlerParameterBinding] and
 * [BodyHandlerParameterSource])
 *
 * [BasicHandlerParameterBinding] supports [ParameterTypeConverter]s for converting
 * the parameter data as obtained from the [HandlerParameterSource] (e.g.
 * path parameters or body) into the desired target type.
 *
 * Many converters use a [Map] as an intermediary type. For example both
 * form and json data is converted first to a [Map] (by [jsonMapDecoder] and
 * [formMapDecoder] respectively) before being converted to their target types
 * typically by [JsonToObjectConverter] and [ConstructorBasedMapToObjectConverter]
 * or similar.
 */
abstract class HandlerParameterBinding {
  final String handlerParameterName;
  final bool isNamed;

  const HandlerParameterBinding(this.handlerParameterName, this.isNamed);
  dynamic createInstance(Request request);
  void validate();
}

/// The most common sort of [HandlerParameterBinding]
///
/// It obtains the data for the handlers parameter from the provided [source] and
/// converts it to the target type with the provided [converter]
class BasicHandlerParameterBinding extends HandlerParameterBinding {
  final HandlerParameterSource source;
  final ParameterTypeConverter converter;

  BasicHandlerParameterBinding(
      String handlerParameterName, bool isNamed, this.source, this.converter)
      : super(handlerParameterName, isNamed);

  dynamic createInstance(Request request) async {
    final value = await source.extractParameter(request);

    return converter.convert(value,
        requestContentType:
            ContentType.lookup(request.mimeType).getOrElse(() => null));
  }

  @override
  void validate() {
//    super.validate();
    ensure(source, isNotNull);
    ensure(converter, isNotNull);
  }
}

/// Extracts the data for the handler parameter from [Request]s
abstract class HandlerParameterSource {
  dynamic extractParameter(Request request);
}

/// A [HandlerParameterSource] that extracts the data from the requests body
class BodyHandlerParameterSource extends HandlerParameterSource {
  @override
  extractParameter(Request request) => request.readAsString();
}

/// A [HandlerParameterSource] that extracts the data from a single path parameter
/// of the request
class PathHandlerParameterSource extends HandlerParameterSource {
  final String pathParameterName;
  final String snakePathParameterName;

  PathHandlerParameterSource(String pathParameterName)
      : this.pathParameterName = pathParameterName,
        this.snakePathParameterName = camelToSnake(pathParameterName);

  @override
  extractParameter(Request request) {
    final result = getPathParameter(request, pathParameterName);
    return result != null
        ? result
        : getPathParameter(request, snakePathParameterName);
  }
}

/// A [HandlerParameterSource] that extracts the data from a single header field
/// of the request
class HeaderHandlerParameterSource extends HandlerParameterSource {
  final String headerName;

  HeaderHandlerParameterSource(this.headerName);

  HeaderHandlerParameterSource.fromCamelCase(String headerName)
      : this(camelToKebab(headerName));

  @override
  extractParameter(Request request) => request.headers[headerName];
}

/// A [HandlerParameterSource] that extracts the data from several path
/// parameters
class MultiPathHandlerParameterSource extends HandlerParameterSource {
  @override
  extractParameter(Request request) => getPathParameters(request);
}

/// Converts the data for a handler parameter into another type
abstract class ParameterTypeConverter {
  dynamic convert(parameterValue, {ContentType requestContentType});
}

/// Converts the data for a handler parameter into another simple type
class SimpleParameterTypeConverter extends ParameterTypeConverter {
  final Type simpleType;

  SimpleParameterTypeConverter([this.simpleType = String]);

//  @override
//  void validate() {
//    ensure(simpleType, isNotNull);
//  }

  @override
  convert(value, {ContentType requestContentType}) {
    return value == null || simpleType == value.runtimeType
        ? value
        : convertTo(simpleType, value);
  }
}

//DAMN: JSON based converters don't need a type binding as it is all handled
// by a special fromJson ctr!!!

/// Converts the data for a handler parameter into another non simple class
abstract class ComplexParameterTypeConverter extends ParameterTypeConverter {
//  final TypeBinding typeBinding;
//
//  ComplexParameterTypeConverter(this.typeBinding);

//  ComplexParameterTypeConverter copy(TypeBinding newTypeBinding);
}

/// decodes a value into a Map
typedef Map ToMapDecoder(value, {ContentType requestContentType});

Map jsonMapDecoder(String str, {ContentType requestContentType}) =>
    JSON.decode(str);
Map formMapDecoder(String str, {ContentType requestContentType}) =>
    Uri.splitQueryString(str);

Map requestContentTypeMapDecoder(String str, {ContentType requestContentType}) {
  ToMapDecoder getDecoder() {
    if (requestContentType == null) {
      return jsonMapDecoder;
    }
    switch (requestContentType) {
      case ContentType.JSON:
        return jsonMapDecoder;
      case ContentType.FORM:
        return formMapDecoder;
      default:
        throw new ArgumentError.value(
            requestContentType, 'content-type', 'unsupported content-type');
    }
  }

  return getDecoder()(str);
}

Map identityMapDecoder(Map map, {ContentType requestContentType}) => map;

// TODO: this should really be a typedef
/// converts a Map into some other Class
abstract class MapToObjectConverter {
  convert(Map map, {ContentType requestContentType});
}

class IdentityMapToObjectConverter implements MapToObjectConverter {
  @override
  convert(Map map, {ContentType requestContentType}) =>
      identityMapDecoder(map, requestContentType: requestContentType);
}

/// A [MapToObjectConverter] that invokes the #fromJson constructor of a
/// given class
class JsonToObjectConverter implements MapToObjectConverter {
  final ClassMirror classMirror;

  JsonToObjectConverter(this.classMirror);

  JsonToObjectConverter.fromType(Type type) : this(reflectClass(type));

  @override
  convert(Map map, {ContentType requestContentType}) {
    final instanceMirror = classMirror.newInstance(#fromJson, [map]);

    return instanceMirror.reflectee;
  }
}

class ContentTypeBasedObjectConverter implements MapToObjectConverter {
  final Map<ContentType, MapToObjectConverter> converters;
  final MapToObjectConverter defaultConverter;

  ContentTypeBasedObjectConverter(this.converters, this.defaultConverter);

  @override
  convert(Map map, {ContentType requestContentType}) {
    MapToObjectConverter getConverter() {
      if (requestContentType == null) {
        return defaultConverter;
      }
      final converter = converters[requestContentType];
      if (converter == null) {
        throw new ArgumentError.value(
            requestContentType, 'content-type', 'unsupported content-type');
      }
      return converter;
    }

    return getConverter().convert(map);
  }
}

abstract class ComplexMapToObjectConverter implements MapToObjectConverter {
  final TypeBinding typeBinding;

  ComplexMapToObjectConverter(this.typeBinding);
}

/// A [MapToObjectConverter] that invokes a constructor (with default name of
/// #build) of given class, mapping the names of constructor parameters,
/// to request path parameters according to the associated [TypeBinding]
class ConstructorBasedMapToObjectConverter extends ComplexMapToObjectConverter {
  ConstructorBasedMapToObjectConverter(TypeBinding typeBinding)
      : super(typeBinding);

  @override
  convert(Map parameters, {ContentType requestContentType}) {
    final namedArguments =
        _bindVariables(parameters, typeBinding.memberBindings);
    final classMirror = reflectClass(typeBinding.type);
    final instanceMirror = classMirror.newInstance(
        typeBinding.constructorName, [], namedArguments);
    return instanceMirror.reflectee;
  }
}

/// A [MapToObjectConverter] that invokes an empty constructor
/// of the given class, mapping the names of property setters
/// to request path parameters according to the associated [TypeBinding]
class SetterBasedMapToObjectConverter extends ComplexMapToObjectConverter {
  SetterBasedMapToObjectConverter(TypeBinding typeBinding) : super(typeBinding);

  @override
  convert(Map parameters, {ContentType requestContentType}) {
    final classMirror = reflectClass(typeBinding.type);
    final instanceMirror = classMirror.newInstance(const Symbol(''), []);
    return convertToInstance(parameters, instanceMirror);
  }

  convertToInstance(Map parameters, constructedInstance) {
    final properties = _bindVariables(parameters, typeBinding.memberBindings);
    final instanceMirror = constructedInstance is InstanceMirror
        ? constructedInstance
        : reflect(constructedInstance);

    properties.forEach((property, value) {
      instanceMirror.setField(property, value);
    });
    return instanceMirror.reflectee;
  }
}

/// A [ComplexParameterTypeConverter] that uses a [Map] as an intermediate
/// representation of the object. It uses an associated [mapDecoder] to first
/// convert the data from the source form (typically a String) into a [Map]
/// then passes the map into the associated [converter] to turn it into an
/// object of the target type
class MapBasedParameterTypeConverter extends ComplexParameterTypeConverter {
  final ToMapDecoder mapDecoder;
  final MapToObjectConverter converter;

  MapBasedParameterTypeConverter(this.mapDecoder, this.converter);

  @override
  convert(parameterValue, {ContentType requestContentType}) => convertMap(
      mapDecoder(parameterValue, requestContentType: requestContentType),
      requestContentType);

  convertMap(Map map, ContentType requestContentType) =>
      converter.convert(map, requestContentType: requestContentType);

//  @override
//  MapBasedParameterTypeConverter copy(TypeBinding newTypeBinding) =>
//      new MapBasedParameterTypeConverter(newTypeBinding, mapDecoder, converter);
}

typedef CustomObjectFactory(Request request);

/// A [HandlerParameterBinding] that binds to a provided custom object using
/// the associated [CustomObjectFactory]
class CustomObjectParameterBinding extends HandlerParameterBinding {
  final CustomObjectFactory factory;

  const CustomObjectParameterBinding(String handlerParameterName, this.factory)
      : super(handlerParameterName, false);

  @override
  void validate() {}

  @override
  createInstance(Request request) => factory(request);
}

Request _noopCustomObjectFactory(request) => request;

/// A special [CustomObjectParameterBinding] for the [Request] class
class RequestObjectParameterBinding extends CustomObjectParameterBinding {
  RequestObjectParameterBinding([String handlerParameterName = 'request'])
      : super(handlerParameterName, _noopCustomObjectFactory);
}

Map<Symbol, dynamic> _bindVariables(
    Map<String, Object> parameters, Iterable<MemberBinding> memberBindings) {
  final convertedPv = <Symbol, Object>{};
  parameters.forEach((k, v) {
    convertedPv[new Symbol(k)] = v;
  });

  final namedArguments = <Symbol, dynamic>{};
  memberBindings.forEach((mb) {
    final value = convertedPv[mb.pathParameterName];
    if (value != null) {
      namedArguments[mb.pathParameterName] = mb.convert(value);
    }
  });

  return namedArguments;
}
