// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.response;

import 'package:shelf/shelf.dart' show Request, Response;
import 'dart:mirrors' show ClosureMirror, InstanceMirror;
import 'dart:convert' show JSON;
import 'package:path/path.dart' as p show url;
import 'dart:io' show HttpStatus;

final _url = p.url;

abstract class ResponseBinding {
  const ResponseBinding();

  createResponse(Request request, InstanceMirror responseMirror);
}

abstract class BaseResponseBinding extends ResponseBinding {
  final int successStatus;
  final bool setLocation;
  final Symbol idField;

  const BaseResponseBinding(this.successStatus,
      {this.setLocation: false, this.idField: #id});

  const BaseResponseBinding.ok() : this(HttpStatus.OK);

  const BaseResponseBinding.created()
      : this(HttpStatus.CREATED, setLocation: true);

  createResponse(Request request, InstanceMirror responseMirror) {
    final r = responseMirror.reflectee;
    if (r == null) {
      return new Response.internalServerError(
          body: 'handler method returned null');
    }

    if (r is Response) {
      return r;
    }

    return doCreateResponse(request, responseMirror);
  }

  Response doCreateResponse(Request request, InstanceMirror responseMirror) {
    final body = createBody(request, responseMirror);
    final statusCode = createStatusCode(request, responseMirror, body);
    final headers = createHeaders(request, responseMirror, body);
    return new Response(statusCode, body: body, headers: headers);
  }

  createBody(Request request, InstanceMirror responseMirror);

  int createStatusCode(
          Request request, InstanceMirror responseMirror, dynamic body) =>
      successStatus;

  Map<String, String> createHeaders(
      Request request, InstanceMirror responseMirror, dynamic body) {
    final headers = {};
    if (setLocation) {
      final location = _getLocation(request, responseMirror);
      headers['Location'] = location;
    }
    addHeaders(headers, request, responseMirror, body);
    return headers;
  }

  String _getLocation(Request request, InstanceMirror responseMirror) {
    if (request.method != 'POST') {
      return request.requestedUri.toString();
    }

    final String id = getId(request, responseMirror);
    final requestPath = _url.fromUri(request.requestedUri);
    return _url.join(requestPath, id);
  }

  String getId(Request request, InstanceMirror responseMirror) {
    final id = responseMirror.getField(idField);
    return id.reflectee;
  }

  void addHeaders(Map<String, String> headers, Request request,
      InstanceMirror responseMirror, dynamic body) {}
}

class JsonResponseBinding extends BaseResponseBinding {
  const JsonResponseBinding(int successStatus,
      {bool setLocation: false, Symbol idField: #id})
      : super(successStatus, setLocation: setLocation, idField: idField);

  const JsonResponseBinding.ok() : super.ok();

  const JsonResponseBinding.created() : super.created();

  @override
  createBody(Request request, InstanceMirror responseMirror) =>
      JSON.encode(responseMirror.reflectee);

  @override
  void addHeaders(Map<String, String> headers, Request request,
      InstanceMirror responseMirror, dynamic body) {
    headers['Content-Type'] = 'application/json';
  }
}
