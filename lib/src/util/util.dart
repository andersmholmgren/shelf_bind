library shelf_bind.util;

Iterable<Map> iterableToJson(Iterable i) =>
    i == null ? null : i.map((j) => j.toJson()).toList(growable: false);
