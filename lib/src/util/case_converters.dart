// Copyright (c) 2015, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.convert.stringcase;

import 'package:quiver/iterables.dart';

String snakeToCamel(String str) => _toCamel(str, '_');

String kebabToCamel(String str) => _toCamel(str, '-');

String camelToSnake(String str) => _fromCamel(str, '_');

String camelToKebab(String str) => _fromCamel(str, '-');

String capitalise(String str) {
  if (str == null || str.length < 2) {
    return str;
  }

  return str.substring(0, 1).toUpperCase() + str.substring(1);
}

String _toCamel(String str, String separator) {
  if (str == null || str.isEmpty) {
    return str;
  }

  final segments = str.split(separator);
  if (segments.length <= 1) {
    return str;
  }

  final first = segments.first;
  final remainder = segments.skip(1).map(capitalise);
  return concat([
    [first],
    remainder
  ]).join('');
}

String _fromCamel(String str, String separator) {
  if (str == null || str.length < 2) {
    return str;
  }

  int start = 0;

  final segments = <String>[];

  for (int i = 0; i < str.length; i++) {
    final char = str.substring(i, i + 1);
    final isUpper = char.toUpperCase() == char && char.toLowerCase() != char;
    if (isUpper) {
      segments.add(str.substring(start, i));
      start = i;
    }
  }

  segments.add(str.substring(start, str.length));

  return segments.map((s) => s.toLowerCase()).join(separator);

//  final segments = str.split(separator);
//  if (segments.length <= 1) {
//    return str;
//  }
//
//  final first = segments.first;
//  final remainder = segments.skip(1).map(capitalise);
//  return concat([[first], remainder]).join('');
}

main() {
  print("_${'_'.toUpperCase()}");
  print("-${'-'.toUpperCase()}");
  print('-' == '-'.toUpperCase());
  print('-' == '-'.toLowerCase());
  print(camelToSnake('my-foo'));
}
