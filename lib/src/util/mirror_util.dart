// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library mirror.util;

import 'dart:mirrors';
import 'package:option/option.dart';

dynamic getAnnotation(DeclarationMirror dm, Type annotationType) {
  final typeMirror = reflectType(annotationType);
  final resourceAnnotations =
      dm.metadata.where((md) => typeMirror.isAssignableTo(md.type));
  if (resourceAnnotations.isEmpty) {
    return null;
  }

  return resourceAnnotations.first.reflectee;
}

Option<dynamic> getAnnotationOpt(DeclarationMirror dm, Type annotationType) =>
    new Option(getAnnotation(dm, annotationType));

MethodMirror getMethodMirror(InstanceMirror resource, String methodName) {
  final dm = resource.type.declarations[new Symbol(methodName)];
  if (dm == null) {
    return null;
  }
  if (dm is MethodMirror && dm.isRegularMethod) {
    return dm;
  }

  return null;
}

Map<Symbol, MethodMirror> getMethodMirrors(InstanceMirror resource) {
  final methods = <Symbol, MethodMirror>{};

  resource.type.declarations.forEach((k, v) {
    if (v is MethodMirror && v.isRegularMethod) {
      methods[k] = v;
    }
  });

  return methods;
}

//Map<Symbol, MethodMirror> getMethodMirrors(InstanceMirror resource) {

// TODO: Not a good way to determine this
bool isSimpleType(TypeMirror tm) =>
//!tm.isSubtypeOf(reflectClass(Map)) &&
//    !tm.isSubtypeOf(reflectClass(Iterable)) &&
    (tm.isTopLevel &&
        (tm.reflectedType == dynamic || tm.owner.simpleName == #dart.core));
