// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.param.inferencer;

import 'package:shelf_bind/src/model/parameter_bindings.dart';
import 'dart:mirrors';
import 'package:shelf_bind/src/util/mirror_util.dart';
import 'package:shelf_bind/src/model/annotations.dart';
import 'package:shelf_bind/src/model/type_bindings.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_bind/src/inferencing/type_bindings_inferencer.dart';

final ParameterBindingInferencer _defaultParameterInferencer =
    new ParameterBindingInferencer();

Map _mapValues(Map map, convert(v)) {
  final result = {};
  map.forEach((k, v) => result[k] = convert(v));
  return result;
}

List<HandlerParameterBinding> inferHandlerParameterBindings(
    ClosureMirror functionMirror,
    Map<Type, CustomObjectFactory> customObjects) {
  final ParameterBindingInferencer bindingInferencer = customObjects == null
      ? _defaultParameterInferencer
      : new ParameterBindingInferencer(
          additionalBindingFactories: _mapValues(
              customObjects,
              (factory) => (String name) =>
                  new CustomObjectParameterBinding(name, factory)));

  return bindingInferencer.inferHandlerParameterBindings(functionMirror);
}

typedef HandlerParameterBinding HandlerParameterBindingFactory(String name);

class ParameterBindingInferencer {
  static final Map<Type, HandlerParameterBindingFactory>
      _defaultBindingFactories = {
    Request: (String name) => new RequestObjectParameterBinding(name)
  };

  final Map<Type, HandlerParameterBindingFactory> bindingFactories;

  ParameterBindingInferencer(
      {Map<Type, HandlerParameterBindingFactory> additionalBindingFactories})
      : this.bindingFactories = additionalBindingFactories != null
            ? ({}
              ..addAll(additionalBindingFactories)
              ..addAll(_defaultBindingFactories))
            : _defaultBindingFactories;

  List<HandlerParameterBinding> inferHandlerParameterBindings(
      ClosureMirror functionMirror) {
    final params = functionMirror.function.parameters;
    final inferredBindings = <HandlerParameterBinding>[];

    for (int i = 0; i < params.length; i++) {
      final param = params[i];
      final parameterBinding = _inferHandlerParameterBinding(param);
      if (parameterBinding != null) {
        inferredBindings.add(parameterBinding);
      }
    }

    final int numBodyBindings = inferredBindings
        .where((b) =>
            b is BasicHandlerParameterBinding &&
            b.source is BodyHandlerParameterSource)
        .length;

    if (numBodyBindings > 1) {
      throw new ArgumentError(
          'only one handler parameter can bind to the request body');
    }
    return inferredBindings;
  }

  HandlerParameterBinding _inferHandlerParameterBinding(ParameterMirror param) {
    final functionParameterName = MirrorSystem.getName(param.simpleName);

    // TODO: better name than defaultBinding
    final defaultBindingFactory = bindingFactories[param.type.reflectedType];
    if (defaultBindingFactory != null) {
      return defaultBindingFactory(functionParameterName);
    }

    final isSimple = isSimpleType(param.type);

    final RequestHandlerParam requestHandlerParam =
        _inferRequestHandlerParam(isSimple, param, RequestBody);

    final HandlerParameterSource source = _inferParameterSource(
        isSimple, requestHandlerParam, functionParameterName);

    final ParameterTypeConverter converter =
        _inferParameterConverter(param, isSimple, requestHandlerParam);

    return new BasicHandlerParameterBinding(
        functionParameterName, param.isNamed, source, converter);
  }

  RequestHandlerParam _inferRequestHandlerParam(
      bool isSimple, ParameterMirror param, Type requestBody) {
    final RequestHandlerParam providedAnnotation =
        getAnnotation(param, RequestHandlerParam);
    if (providedAnnotation != null) {
      return providedAnnotation;
    } else if (isSimple && !_isJsonType(param)) {
      return new PathParam();
    } else {
      // set format to null as want to default to using request contentType
      return new RequestBody(format: null);
    }
  }

  ParameterTypeConverter _inferParameterConverter(ParameterMirror param,
      bool isSimple, RequestHandlerParam requestHandlerParam) {
    // hmmm cludgy
    final useSimple = isSimple &&
        (requestHandlerParam == null ||
            requestHandlerParam is! RequestBody ||
            ((requestHandlerParam as RequestBody).format ?? ContentType.JSON) ==
                ContentType.JSON);

    if (useSimple) {
      return new SimpleParameterTypeConverter(param.type.reflectedType);
    } else {
      final decoder = _inferDecoder(requestHandlerParam);
      final providedBinding = _inferProvidedTypeBinding(requestHandlerParam);
      final inferredTypeBinding = inferTypeBinding(providedBinding, param);

      final converter = _inferObjectConverter(
          requestHandlerParam, inferredTypeBinding, param.type.reflectedType);
      return new MapBasedParameterTypeConverter(decoder, converter);
    }
  }

  TypeBinding _inferProvidedTypeBinding(
      RequestHandlerParam requestHandlerParam) {
    return requestHandlerParam is PathParams
        ? new TypeBinding.fromAnnotation(requestHandlerParam)
        : new TypeBinding(null);
  }

  HandlerParameterSource _inferParameterSource(bool isSimple,
      RequestHandlerParam requestHandlerParam, String functionParameterName) {
    if (requestHandlerParam is RequestBody) {
      return new BodyHandlerParameterSource();
    } else if (requestHandlerParam is PathParam) {
      final pathParamName = requestHandlerParam.pathName != null
          ? requestHandlerParam.pathName
          : functionParameterName;
      return new PathHandlerParameterSource(pathParamName);
    } else if (requestHandlerParam is PathParams) {
      return new MultiPathHandlerParameterSource();
    } else if (requestHandlerParam is HeaderParam) {
      final headerName = requestHandlerParam.headerName != null
          ? requestHandlerParam.headerName
          : functionParameterName;
      return new HeaderHandlerParameterSource.fromCamelCase(headerName);
    } else {
      throw new StateError(
          'unhandled RequestHandlerParam type $requestHandlerParam');
    }
  }

  ToMapDecoder _inferDecoder(RequestHandlerParam requestHandlerParam) {
    if (requestHandlerParam is RequestBody) {
      if (requestHandlerParam.format == null) {
        return requestContentTypeMapDecoder;
      }
      switch (requestHandlerParam.format) {
        case ContentType.JSON:
          return jsonMapDecoder;
        case ContentType.FORM:
          return formMapDecoder;

        default:
          throw new ArgumentError('unsupported content type for decoding: '
              '${requestHandlerParam.format}');
      }
    } else {
      return identityMapDecoder;
    }
  }

  MapToObjectConverter _inferObjectConverter(
      RequestHandlerParam requestHandlerParam, TypeBinding binding, Type type) {
    MapToObjectConverter complexConverter() {
      final bindMode = binding.bindMode;
      switch (bindMode) {
        case BindMode.CONSTRUCTOR:
          return new ConstructorBasedMapToObjectConverter(binding);
        case BindMode.PROPERTY:
          return new SetterBasedMapToObjectConverter(binding);
        case BindMode.CONSTRUCTOR_THEN_PROPERTY:
          throw new StateError('should not get CONSTRUCTOR_THEN_PROPERTY here');
        default:
          throw new ArgumentError('unsupported bindMode: ${bindMode}');
      }
    }

    if (requestHandlerParam is RequestBody) {
      if (requestHandlerParam.format == null) {
        final jsonConverter = new JsonToObjectConverter.fromType(binding.type);
        final formConverter = complexConverter();

        return new ContentTypeBasedObjectConverter(
            {ContentType.JSON: jsonConverter, ContentType.FORM: formConverter},
            jsonConverter);
      }

      switch (requestHandlerParam.format) {
        case ContentType.JSON:
          return new JsonToObjectConverter.fromType(binding.type);
        case ContentType.FORM:
          return type == Map
              ? new IdentityMapToObjectConverter()
              : complexConverter();

        default:
          throw new ArgumentError(
              'unsupported content type for converting: ${requestHandlerParam.format}');
      }
    } else if (requestHandlerParam is PathParams) {
      return complexConverter();
    } else {
      throw new StateError('object conversion not support for simple types');
    }
  }
}

final _mapTypeMirror = reflectClass(Map);
final _listTypeMirror = reflectClass(List);

final _jsonTypeMirrors = [_mapTypeMirror, _listTypeMirror];

bool _isJsonType(ParameterMirror param) => _jsonTypeMirrors.any(
    (tm) => param.type.reflectedType != dynamic && param.type.isSubtypeOf(tm));
