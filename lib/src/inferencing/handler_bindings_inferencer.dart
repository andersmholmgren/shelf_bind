// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.handler.inferencer;

import 'package:shelf_bind/src/model/parameter_bindings.dart';
import 'package:shelf_bind/src/model/handler_bindings.dart';
import 'package:matcher/matcher.dart';
import 'package:shelf_bind/src/util/preconditions.dart';
import 'dart:mirrors';
import 'package:shelf_bind/src/inferencing/parameter_bindings_inferencer.dart';
import 'package:shelf_bind/src/inferencing/response_bindings_inferencer.dart';

HandlerBinding createHandlerBinding(
    Function handler,
    Map<Type, CustomObjectFactory> customObjects,
    bool validateParameters,
    bool validateReturn) {
  ensure(handler, isNotNull, 'Cannot bind to a null handler');
  return _inferHandlerBindings(
      handler, customObjects, validateParameters, validateReturn);
}

HandlerBinding _inferHandlerBindings(
    Function handler,
    Map<Type, CustomObjectFactory> customObjects,
    bool validateParameters,
    bool validateReturn) {
  final ClosureMirror functionMirror = reflect(handler);
  final inferredParameterBindings =
      inferHandlerParameterBindings(functionMirror, customObjects);

  final inferredResponseBinding = inferResponseBinding(functionMirror);

  return new HandlerBinding(inferredParameterBindings, handler,
      inferredResponseBinding, validateParameters, validateReturn);
}
