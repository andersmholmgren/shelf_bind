// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.response.inferencer;

import 'dart:mirrors' show ClosureMirror, InstanceMirror;
import 'package:path/path.dart' as p show url;
import 'package:shelf_bind/src/model/response_bindings.dart';
import 'package:shelf_bind/src/model/annotations.dart';
import 'package:shelf_bind/src/util/mirror_util.dart';

final _url = p.url;

ResponseBinding inferResponseBinding(ClosureMirror functionMirror) {
  ResponseHeaders responseHeaders =
      getAnnotation(functionMirror.function, ResponseHeaders);

  return responseHeaders != null
      ? new JsonResponseBinding(responseHeaders.successStatus,
          setLocation: responseHeaders.setLocation,
          idField: responseHeaders.idField)
      : new JsonResponseBinding.ok();
}
