// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.type.inference;

import 'package:shelf/shelf.dart' show Request;
import 'dart:mirrors';
import 'package:shelf_bind/src/model/annotations.dart';
import 'package:shelf_bind/src/util/mirror_util.dart';
import 'package:shelf_bind/src/model/parameter_bindings.dart';
import 'package:shelf_bind/src/model/type_bindings.dart';

TypeBinding inferTypeBinding(TypeBinding providedBinding, ParameterMirror pm) {
  final parameterType = pm.type;

  final constructorName = providedBinding.constructorName;
  final bindMode = providedBinding.bindMode;

  final typeMirror = _inferType(providedBinding.type, parameterType);
  if (!(typeMirror is ClassMirror)) {
    return null;
  }

  final cm = typeMirror;

  if (isSimpleType(cm) ||
      cm.isSubtypeOf(reflectType(Map)) ||
      cm.isSubtypeOf(reflectType(Iterable))) {
    return null;
  }

  if (cm.reflectedType == Request) {
    return null;
  }

  final type = typeMirror.reflectedType;

  final bindModeAndBindings = _inferMemberBindings(
      providedBinding.memberBindings, typeMirror, bindMode, constructorName);

  return new TypeBinding.def(type, constructorName,
      bindModeAndBindings.bindMode, bindModeAndBindings.memberBindings);
}

TypeMirror _inferType(Type providedType, TypeMirror parameterType) {
  return providedType != null ? reflectType(providedType) : parameterType;
}

class _ModeBindingPair {
  final BindMode bindMode;
  final Iterable<MemberBinding> memberBindings;

  _ModeBindingPair(this.bindMode, this.memberBindings);
}

_ModeBindingPair _inferMemberBindings(Iterable<MemberBinding> providedBindings,
    ClassMirror parameterType, BindMode bindMode, Symbol constructorName) {
  switch (bindMode) {
    case BindMode.CONSTRUCTOR_THEN_PROPERTY:
      return _inferBindingsViaConstructorThenProperty(
          providedBindings, parameterType, constructorName);
    case BindMode.CONSTRUCTOR:
      return new _ModeBindingPair(
          BindMode.CONSTRUCTOR,
          _inferMemberBindingsViaConstructor(
              providedBindings, parameterType, constructorName));
    case BindMode.PROPERTY:
      return new _ModeBindingPair(BindMode.PROPERTY,
          _inferMemberBindingsViaProperty(providedBindings, parameterType));

    default:
      throw new StateError('unsupported bind mode: ${bindMode}');
  }
}

_ModeBindingPair _inferBindingsViaConstructorThenProperty(
    Iterable<MemberBinding> providedBindings,
    ClassMirror parameterType,
    Symbol constructorName) {
  final viaCtr = _inferMemberBindingsViaConstructor(
      providedBindings, parameterType, constructorName,
      throwOnMissingCtr: false);

  final bindMode = viaCtr != null ? BindMode.CONSTRUCTOR : BindMode.PROPERTY;

  final inferredBindings = viaCtr != null
      ? viaCtr
      : _inferMemberBindingsViaProperty(providedBindings, parameterType);

  return new _ModeBindingPair(bindMode, inferredBindings);
}

Iterable<MemberBinding> _inferMemberBindingsViaConstructor(
    Iterable<MemberBinding> providedBindings,
    ClassMirror parameterType,
    Symbol constructorName,
    {throwOnMissingCtr: true}) {
//  final BiIterable<MemberBinding> _providedBindings =
//    new BiIterable<MemberBinding>()..addAll(providedBindings);

  final fullCtrName =
      new Symbol("${MirrorSystem.getName(parameterType.simpleName)}"
          ".${MirrorSystem.getName(constructorName)}");

  final MethodMirror ctrMirror = parameterType.declarations[fullCtrName];
  if (ctrMirror == null) {
    if (throwOnMissingCtr) {
      throw new ArgumentError('No such constructor $fullCtrName');
    } else {
      return null;
    }
  }

  if (!ctrMirror.isConstructor) {
    throw new ArgumentError('$fullCtrName is not a constructor');
  }

  final memberBindings =
      ctrMirror.parameters.map((b) => _inferMemberBinding(b, providedBindings));

  return memberBindings;
}

Iterable<MemberBinding> _inferMemberBindingsViaProperty(
    Iterable<MemberBinding> providedBindings, ClassMirror parameterType) {
  final memberBindings = [];

  parameterType.declarations.forEach((s, dm) {
    if (dm is VariableMirror || (dm is MethodMirror && dm.isSetter)) {
      memberBindings.add(_inferMemberBinding(dm, providedBindings));
    }
  });

  return memberBindings;
}

MemberBinding _inferMemberBinding(
    DeclarationMirror pm, Iterable<MemberBinding> providedBindings) {
  if (pm is! VariableMirror && !(pm is MethodMirror && pm.isSetter)) {
    throw new ArgumentError('invalid declaration mirror $pm');
  }

  final parameterName = pm.simpleName;
  final providedBinding = providedBindings
      .firstWhere((b) => b.memberName == parameterName, orElse: () => null);

  final type = pm is VariableMirror
      ? pm.type.reflectedType
      : pm is MethodMirror ? pm.parameters[0].type.reflectedType : null;

  assert(type != null);

  final defaultConverter = new SimpleParameterTypeConverter(type);

  if (providedBinding != null) {
    final pathParameterName = providedBinding.pathParameterName != null
        ? providedBinding.pathParameterName
        : parameterName;

    final typeConverter = providedBinding.converter != null
        ? providedBinding.converter
        : defaultConverter;

    return new MemberBinding(pathParameterName, parameterName, typeConverter);
  } else {
    return new MemberBinding(parameterName, parameterName, defaultConverter);
  }
}
