// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.core;

import 'package:shelf/shelf.dart';
import 'package:shelf_route/shelf_route.dart';
import 'models.dart';
import 'package:shelf_bind/src/inferencing/handler_bindings_inferencer.dart';

/**
 * Specify [customObjects] to support injection of arbitrary objects into
 * your handler functions. When the binders encounter such parameters in your
 * function it will call the factory you provide in [customObjects] instead of
 * performing the normal binding process.
 *
 * This is typically used for injecting objects that aren't simply a
 * representation of the data that is contained in the request, but rather some
 * auxiliary object, such as an authenticated client to some service. The
 * construction of that client typically depends on properties of the request
 * to establish authentication.
 *
 * ```
 * // a factory for your client
 * FooClient createFooClient(Request request) => ... create client somehow
 *
 * // pass it as a factory for your client using [customObjects] parameter
 * to [handlerAdapter]
 * var myHandlerAdapter = handlerAdapter(customObjects:
 *    { FooClient: createFooClient });
 *
 * var myRouter = router(handlerAdapter: myHandlerAdapter);
 *
 * myRouter.get('/person/{name}', (FooClient fooClient, String name) => ...);
 * ```
 *
 * [validateParameters] and [validateReturn] have the same meanings as for
 * the [bind] function. They will apply to all methods bound via this adapter
 */
HandlerAdapter handlerAdapter(
    {Map<Type, CustomObjectFactory> customObjects: const {},
    bool validateParameters: false,
    bool validateReturn: false}) {
  return new BoundHandlerAdapter(
      customObjects: customObjects,
      validateParameters: validateParameters,
      validateReturn: validateReturn);
}

/**
 * Bind properties (path parameters, body) of a [Request] to the parameters
 * of a [handler] function.
 *
 * The bindings will be inferred via mirrors, following the conventions of
 * `shelf_bind`.
 *
 * Bindings can be tweaked using annotations like `@PathParam`, `@PathParams`,
 * `@BodyRequest` and `@HeaderParam`
 *
 * The default behaviour for inferring the bindings between the request and the
 * handler parameters is as follows:
 *
 *     * handler parameters that are not simple types are by default assumed to
 *   come from the body.
 *         * this includes `Map`, `List` and any of your classes (that are not
 *         registered as `custom objects`).
 *         * If no annotation is added to the parameter then `shelf_bind` will first
 *     look at the request `content-type`
 *             * if none is present it defaults to `JSON`
 *             * if the `content-type` is set and is `FORM` or `JSON` then it will
 *             be handled as that type
 *             * if it is any other `content-type` then a `400` response will be
 *             returned
 *    * You can map multiple path parameters into your own custom class by using
 *    the `@PathParams` annotation
 *    * there is automatic conversion of snake case (e.g. `account_id`) in query
 *    parameters to camel case (e.g `accountId`) handler parameters
 *    * you can bind to header fields using the new `@HeaderParam` annotation
 *    * there is automatic conversion of kebab case (e.g. `content-type`) in
 *    header fields to camel case (e.g. `contentType`) handler parameters
 *
 * If [validateParameters] is set to true then the [constrain] package will be
 * used to validate all handler parameters (except simple parameters).
 * If validation fails then a [BadRequestException] will be thrown with the
 * constraint violations included in the error. This is useful as preconditions
 * to your handlers so that your handler code knows it's dealing with valid
 * data.
 *
 * If [validateReturn] is set to true then the [constrain] package will be
 * used to validate the result object before it is returned. Similar handling
 * as per [validationParameters] occurs on validation failure. This is useful
 * as a post condition on your handlers to make sure you don't return invalid
 * objects.
 */
Handler bind(Function handler,
    {Map<Type, CustomObjectFactory> customObjects,
    bool validateParameters: false,
    bool validateReturn: false}) {
  return createHandlerBinding(
      handler, customObjects, validateParameters, validateReturn).handler;
}

/**
 * Bind properties (path parameters, body) of a [Request] to the parameters
 * of a [handler] function, by manually providing all the bindings.
 *
 * No binding inference is performed if you call this function.
 *
 * In almost all cases it is better to call `bind` or `handlerAdapter` and
 * use annotations to tweak bindings.
 *
 * This function exists primarily to support other dart packages integrating
 * with `shelf_bind`
 *
 */
Handler bindManually(Function handler,
    {List<HandlerParameterBinding> parameterBindings: const [],
    ResponseBinding responseBinding,
    Map<Type, CustomObjectFactory> customObjects,
    bool validateParameters: false,
    bool validateReturn: false}) {
  return new HandlerBinding(parameterBindings, handler, responseBinding,
      validateParameters, validateReturn).handler;
}
