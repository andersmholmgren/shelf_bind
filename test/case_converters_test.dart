// Copyright (c) 2015, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.convert.stringcase.test;

import 'package:test/test.dart';
import 'package:shelf_bind/src/util/case_converters.dart';

main() {
  group('camelToSnake', () {
    test('null returns null', () {
      expectToSnake(null, null);
    });

    test('empty returns empty', () {
      expectToSnake('', '');
    });

    test('all lower case returns all lower case', () {
      expectToSnake('fred', 'fred');
    });

    test('already snake returns snake', () {
      expectToSnake('fred_barney', 'fred_barney');
    });

    test('camel returns snake', () {
      expectToSnake('fredBarneyWilma', 'fred_barney_wilma');
    });
  });

  group('camelToKebab', () {
    test('null returns null', () {
      expectToKebab(null, null);
    });

    test('empty returns empty', () {
      expectToKebab('', '');
    });

    test('all lower case returns all lower case', () {
      expectToKebab('fred', 'fred');
    });

    test('already kebab returns kebab', () {
      expectToKebab('fred-barney', 'fred-barney');
    });

    test('camel returns kebab', () {
      expectToKebab('fredBarneyWilma', 'fred-barney-wilma');
    });
  });

  group('snakeToCamel', () {
    test('null returns null', () {
      expectFromSnake(null, null);
    });

    test('empty returns empty', () {
      expectFromSnake('', '');
    });

    test('all lower case returns all lower case', () {
      expectFromSnake('fred', 'fred');
    });

    test('already camel returns camel', () {
      expectFromSnake('fredBarney', 'fredBarney');
    });

    test('snake returns camel', () {
      expectFromSnake('fred_barney_wilma', 'fredBarneyWilma');
    });
  });

  group('kebabToCamel', () {
    test('null returns null', () {
      expectFromKebab(null, null);
    });

    test('empty returns empty', () {
      expectFromKebab('', '');
    });

    test('all lower case returns all lower case', () {
      expectFromKebab('fred', 'fred');
    });

    test('already camel returns camel', () {
      expectFromKebab('fredBarney', 'fredBarney');
    });

    test('kebab returns camel', () {
      expectFromKebab('fred-barney-wilma', 'fredBarneyWilma');
    });
  });
}

expectToSnake(String input, String expected) {
  expect(camelToSnake(input), equals(expected));
}

expectToKebab(String input, String expected) {
  expect(camelToKebab(input), equals(expected));
}

expectFromSnake(String input, String expected) {
  expect(snakeToCamel(input), equals(expected));
}

expectFromKebab(String input, String expected) {
  expect(kebabToCamel(input), equals(expected));
}
