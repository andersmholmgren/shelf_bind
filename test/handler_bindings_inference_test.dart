// Copyright (c) 2015, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.handler.inference.test;

import 'package:test/test.dart';

import 'package:shelf_bind/src/model/handler_bindings.dart';
import 'handler_cases.dart';
import 'package:shelf_bind/src/inferencing/handler_bindings_inferencer.dart';
import 'package:shelf_bind/src/model/parameter_bindings.dart';
import 'package:shelf_bind/src/model/type_bindings.dart';
//import 'package:shelf_bind/src/model/type_bindings.dart';

main() {
  handlerCases.forEach(testInference);
}

testInference(HandlerCase handlerCase) {
  group(
      "createHandlerBinding when dart function is '${handlerCase.description}' returns binding that",
      () {
    HandlerBinding expectedHandlerBinding = handlerCase.handlerBinding;
    HandlerBinding inferredHandlerBinding;

//    Future handlerResult() => handler(handlerCase.requestFactory());

    setUp(() {
//      HandlerBinding createHandlerBinding(
//          Function handler,
//          List<HandlerParameterBinding> providedBindings,
//          ResponseBinding responseBinding,
//          Map<Type, CustomObjectFactory> customObjects,
//          bool validateParameters,
//          bool validateReturn) {

      inferredHandlerBinding = createHandlerBinding(handlerCase.handlerFunction,
          {OohLookAtMeImCustom: oohLookAtMeImCustomFactory}, false, false);
    });

    test('is not null', () {
      expect(inferredHandlerBinding, isNotNull);
    });

    group('with parameter bindings', () {
      Iterable<HandlerParameterBinding> inferredParameterBindings;
      Iterable<HandlerParameterBinding> expectedParameterBindings =
          expectedHandlerBinding.parameterBindings;

      setUp(() {
        inferredParameterBindings = inferredHandlerBinding.parameterBindings;
      });

      test('that is not null', () {
        expect(inferredParameterBindings, isNotNull);
      });

      test('of expected length', () {
        expect(inferredParameterBindings,
            hasLength((expectedParameterBindings.length)));
      });

      for (int i = 0; i < expectedParameterBindings.length; i++) {
        group('where parameter no $i', () {
          final expectedParameterBinding =
              expectedParameterBindings.elementAt(i);
          HandlerParameterBinding inferredParameterBinding;

          setUp(() {
            inferredParameterBinding = inferredParameterBindings.elementAt(i);
          });

          test('is expected type', () {
            expect(inferredParameterBinding.runtimeType,
                equals(expectedParameterBinding.runtimeType));
          });

          test('has expected parameter name', () {
            expect(inferredParameterBinding.handlerParameterName,
                equals(expectedParameterBinding.handlerParameterName));
          });

          if (expectedParameterBinding is CustomObjectParameterBinding) {
            CustomObjectParameterBinding expectedCustomObjectBinding =
                expectedParameterBinding;

            test('has expected custom object factory', () {
              expect(
                  expectedCustomObjectBinding.factory,
                  equals(
                      (inferredParameterBinding as CustomObjectParameterBinding)
                          .factory));
            });
          } else if (expectedParameterBinding is BasicHandlerParameterBinding) {
            final BasicHandlerParameterBinding expectedBasicBinding =
                expectedParameterBinding;
            final HandlerParameterSource expectedSource =
                expectedBasicBinding.source;
            final ParameterTypeConverter expectedConverter =
                expectedBasicBinding.converter;

            BasicHandlerParameterBinding inferredBasicBinding;
            HandlerParameterSource inferredSource;
            ParameterTypeConverter inferredConverter;

            group('has basic binding with', () {
              setUp(() {
                inferredBasicBinding = inferredParameterBinding;
                inferredSource = inferredBasicBinding.source;
                inferredConverter = inferredBasicBinding.converter;
              });

              test('expected source type', () {
                expect(inferredSource.runtimeType,
                    equals(expectedSource.runtimeType));
              });

              test('expected converter type', () {
                expect(inferredBasicBinding.converter.runtimeType,
                    equals(expectedBasicBinding.converter.runtimeType));
              });

              testParameterSource(expectedSource, () => inferredSource);

              if (expectedBasicBinding.converter
                  is SimpleParameterTypeConverter) {
                test('expected simple type converter', () {
                  expect(
                      (inferredConverter as SimpleParameterTypeConverter)
                          .simpleType,
                      equals((expectedConverter as SimpleParameterTypeConverter)
                          .simpleType));
                });
              }

              if (expectedBasicBinding.converter
                  is MapBasedParameterTypeConverter) {
                group('expected map based converter with', () {
                  MapBasedParameterTypeConverter expectedMapConverter =
                      expectedConverter;
                  MapBasedParameterTypeConverter inferredMapConverter;

                  setUp(() {
                    inferredMapConverter = inferredConverter;
                  });

                  test('expected mapDecoder', () {
                    expect(inferredMapConverter.mapDecoder,
                        equals(expectedMapConverter.mapDecoder));
                  });

                  testMapBasedConverters(() => inferredMapConverter.converter,
                      expectedMapConverter.converter);

                  // TODO: flesh out for all the different converter types
                });
              }
            });
          }
        });
      }
    });
  });
}

testParameterSource(HandlerParameterSource expectedSource,
    HandlerParameterSource inferredSource()) {
  if (expectedSource is PathHandlerParameterSource) {
    test('expected path parameter name', () {
      expect((inferredSource() as PathHandlerParameterSource).pathParameterName,
          equals(expectedSource.pathParameterName));
    });
  } else if (expectedSource is HeaderHandlerParameterSource) {
    test('expected header name', () {
      expect((inferredSource() as HeaderHandlerParameterSource).headerName,
          equals(expectedSource.headerName));
    });
  } else {
//    fail('unhandled source');
  }
}

testMapBasedConverters(MapToObjectConverter inferredMapConverter(),
    MapToObjectConverter expectedMapConverter) {
  test('expected converter type', () {
    expect(inferredMapConverter().runtimeType,
        equals(expectedMapConverter.runtimeType));
  });

  if (expectedMapConverter is JsonToObjectConverter) {
    test('json converter for expected type', () {
      expect((inferredMapConverter() as JsonToObjectConverter).classMirror,
          equals(expectedMapConverter.classMirror));
    });
  } else if (expectedMapConverter is ComplexMapToObjectConverter) {
    testTypeBindings(
        expectedMapConverter.typeBinding,
        () => (inferredMapConverter() as ComplexMapToObjectConverter)
            .typeBinding);
  } else if (expectedMapConverter is ContentTypeBasedObjectConverter) {
    testContentTypeBasedConverter(
        () => inferredMapConverter(), expectedMapConverter);
  } else if (expectedMapConverter is IdentityMapToObjectConverter) {
    // nothing to do
  } else {
    fail('missing case');
  }
}

testContentTypeBasedConverter(
    ContentTypeBasedObjectConverter inferredObjConverter(),
    ContentTypeBasedObjectConverter expectedObjConverter) {
  group('expected content type based converter with expected', () {
    test('contentTypes', () {
      expect(inferredObjConverter().converters.keys,
          orderedEquals(expectedObjConverter.converters.keys));
    });
    group('expected converters per contentType', () {
      final expectedContentTypeConverters =
          expectedObjConverter.converters.values;
      for (int i = 0; i < expectedContentTypeConverters.length; i++) {
        testMapBasedConverters(
            () => inferredObjConverter().converters.values.elementAt(i),
            expectedContentTypeConverters.elementAt(i));
      }
    });
    group('expected default converter', () {
      testMapBasedConverters(() => inferredObjConverter().defaultConverter,
          expectedObjConverter.defaultConverter);
    });
  });
}

testTypeBindings(
    TypeBinding expectedTypeBinding, TypeBinding inferredTypeBinding()) {
  group('expected complex converter type binding with expected', () {
    test('bindingMode', () {
      expect(inferredTypeBinding().bindMode,
          equals((expectedTypeBinding.bindMode)));
    });

    test('constructorName', () {
      expect(inferredTypeBinding().constructorName,
          equals((expectedTypeBinding.constructorName)));
    });

    test('type', () {
      expect(inferredTypeBinding().type, equals((expectedTypeBinding.type)));
    });

    testMemberBindings(() => inferredTypeBinding().memberBindings,
        expectedTypeBinding.memberBindings);
  });
}

testMemberBindings(Iterable<MemberBinding> inferredMemberBindings(),
    Iterable<MemberBinding> expectedMemberBindings) {
  group('memberBindings', () {
    test('has expected length', () {
      expect(
          inferredMemberBindings(), hasLength(expectedMemberBindings.length));
    });

    for (int i = 0; i < expectedMemberBindings.length; i++) {
      group('binding # $i', () {
        final MemberBinding expectedMemberBinding =
            expectedMemberBindings.elementAt(i);
        MemberBinding inferredMemberBinding;
        setUp(() {
          inferredMemberBinding = inferredMemberBindings().elementAt(i);
        });

        test('has expected memberName', () {
          expect(inferredMemberBinding.memberName,
              equals(expectedMemberBinding.memberName));
        });

        test('has expected pathParameterName', () {
          expect(inferredMemberBinding.pathParameterName,
              equals(expectedMemberBinding.pathParameterName));
        });

        test('has expected converter type', () {
          expect(inferredMemberBinding.converter.runtimeType,
              equals(expectedMemberBinding.converter.runtimeType));
        });

        test('has expected converter with expected simpleType', () {
          expect(
              (inferredMemberBinding.converter as SimpleParameterTypeConverter)
                  .simpleType,
              equals((expectedMemberBinding.converter
                      as SimpleParameterTypeConverter)
                  .simpleType));
        });
      });
    }
  });
}

//compareParameterBinding(Iterable<HandlerParameterBinding> inferredParameterBindings,
//Iterable<HandlerParameterBinding> expectedParameterBindings, int index)
//{
//
//}
