// Copyright (c) 2015, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.handler.test;

import 'package:test/test.dart';

import 'package:shelf/shelf.dart';
import 'dart:async';
import 'handler_cases.dart';

main() {
  handlerCases.forEach(testHandlerExecution);
}

testHandlerExecution(HandlerCase handlerCase) {
  group("handler when dart function is '${handlerCase.description}'", () {
    Handler handler() => handlerCase.handlerBinding.handler;

    test('is not null', () {
      expect(handler(), isNotNull);
    });

    handlerCase.executionCases.forEach((executionCase) {
      testExecutionCase(handler, executionCase);
    });
  });
}

testExecutionCase(Handler handler(), ExecutionCase executionCase) {
  group("for case '${executionCase.description}'", () {
    Future handlerResult() => handler()(executionCase.requestFactory());

    test('returns non null result', () async {
      expect(await handlerResult(), isNotNull);
    });

    test('returns a result that is a Response', () async {
      expect(await handlerResult(), new isInstanceOf<Response>());
    });

    test('passes expected request to dart function', () async {
      expect(await (await handlerResult() as Response).readAsString(),
          equals(executionCase.expectedReturn));
    });
  });
}
