import 'package:shelf/shelf.dart';
import 'dart:async';
import 'package:shelf_bind/src/model/parameter_bindings.dart';
import 'package:shelf_bind/src/model/response_bindings.dart';
import 'package:shelf_path/shelf_path.dart';
import 'package:shelf_bind/src/model/handler_bindings.dart';
import 'package:shelf_bind/src/model/annotations.dart';
import 'package:shelf_bind/src/model/type_bindings.dart';
import 'package:quiver/iterables.dart';

Iterable<HandlerCase> handlerCases = [
  dynamicCase,
  vanillaCase,
  vanillaFutureCase,
  stringsCase,
  simpleTypesCase,
  optionalNamedParametersCase,
  jsonMapTypeCase,
  jsonListTypeCase,
  formMapTypeCase,
  classTypeFromJsonBodyCase,
  classTypeFromFormBodyCase,
  classTypeFromContentTypeBodyCase,
  classTypeFromParamsCase,
  classTypeWithSettersFromParamsCase,
  customObjectCase,
  headerParametersCase
];

String _returnVal(Iterable i) => i.map((e) => e.toString()).join('-');

Response vanilla(Request request) =>
    new Response.ok(request.requestedUri.toString());

HandlerCase vanillaCase = new HandlerCase(
    'Normal Shelf Handler',
    vanilla,
    createRequestFactory('foo?blah=frr'),
    'http://example.com/foo?blah=frr',
    [new RequestObjectParameterBinding()]);

Future<Response> vanillaFuture(Request request) async =>
    await new Response.ok(request.requestedUri.toString());

HandlerCase vanillaFutureCase = new HandlerCase(
    'Normal Shelf Handler Returning a Future',
    vanillaFuture,
    createRequestFactory('foo?blah=frr'),
    'http://example.com/foo?blah=frr',
    [new RequestObjectParameterBinding()]);

String strings(@PathParam(pathName: 'ArgOne') String arg1, String argTwo) =>
    _returnVal([arg1, argTwo]);

HandlerCase stringsCase = new HandlerCase(
    'A Handler with string parameters and return',
    strings,
    createRequestFactory('foo',
        pathParams: {'ArgOne': 'val1', 'arg_two': 'val2'}),
    '"val1-val2"',
    [pathParam('arg1', String, false, 'ArgOne'), pathParam('argTwo', String)]);

dyn(@PathParam(pathName: 'ArgOne') arg1, dynamic argTwo) =>
    _returnVal([arg1, argTwo]);

HandlerCase dynamicCase = new HandlerCase(
    'A Handler with dynamic parameters and return',
    dyn,
    createRequestFactory('foo',
        pathParams: {'ArgOne': 'val1', 'arg_two': 'val2'}),
    '"val1-val2"',
    [
      pathParam('arg1', dynamic, false, 'ArgOne'),
      pathParam('argTwo', dynamic)
    ]);

String simpleTypes(
        int arg1, double arg2, bool arg3, DateTime arg4, Uri arg5, num arg6) =>
    _returnVal([arg1, arg2, arg3, arg4, arg5, arg6]);

HandlerCase simpleTypesCase = new HandlerCase(
    'A handler with simple type args and return',
    simpleTypes,
    createRequestFactory('foo', pathParams: {
      'arg1': '1',
      'arg2': '2.2',
      'arg3': 'true',
      'arg4': new DateTime.utc(2000).toString(),
      'arg5': 'http://localhost',
      'arg6': '6'
    }),
    '"1-2.2-true-2000-01-01 00:00:00.000Z-http://localhost-6"',
    [
      pathParam('arg1', int),
      pathParam('arg2', double),
      pathParam('arg3', bool),
      pathParam('arg4', DateTime),
      pathParam('arg5', Uri),
      pathParam('arg6', num)
    ],
    null,
    [
      execution('missing params', createRequestFactory('foo'),
          '"null-null-null-null-null-null"')
    ]);

String optionalNamedParameter({int arg1: 1, int arg2: 2}) =>
    _returnVal([arg1, arg2]);

HandlerCase optionalNamedParametersCase = new HandlerCase(
    'A handler with optional named parameters',
    optionalNamedParameter,
    createRequestFactory('foo', pathParams: {'arg2': '5'}),
    '"1-5"',
    [pathParam('arg1', int, true), pathParam('arg2', int, true)]);

Map jsonMapType(Map arg1) => arg1;

HandlerCase jsonMapTypeCase = new HandlerCase(
    'A Handler with JSON Map body and return',
    jsonMapType,
    createRequestFactory('foo', body: '{"arg1": 1}'),
    '{"arg1":1}',
    [simpleBodyParam('arg1', Map)]);

HandlerCase formMapTypeCase = new HandlerCase(
    'A Handler with FORM Map body and JSON return',
    mapTypeFromFormBody,
    createRequestFactory('foo', body: 'arg1=1'),
    '{"arg1":"1"}',
    [formMapBodyParam('arg1')]);

List jsonListType(List arg1) => arg1;

HandlerCase jsonListTypeCase = new HandlerCase(
    'A Handler with JSON List body and return',
    jsonListType,
    createRequestFactory('foo', body: '["val1"]'),
    '["val1"]',
    [simpleBodyParam('arg1', List)]);

// TODO: add cases for
// - use annotations to override things

//TODO: error case two body mappings
//Map jsonTwoMapsType(Map arg1, Map arg2) => arg1;

Account classTypeFromContentTypeBody(Account account) => account;

HandlerCase classTypeFromContentTypeBodyCase = new HandlerCase(
    'A Handler with dart class body determined from contentType',
    classTypeFromContentTypeBody,
    createRequestFactory('foo', body: '{"accountNumber": 10}'),
    '{"accountNumber":10}',
    [
      contentTypeClassBodyParam('account', Account, [
        new MemberBinding(#accountNumber, #accountNumber,
            new SimpleParameterTypeConverter(int))
      ])
    ],
    null,
    [
      execution(
          'JSON content-type',
          createRequestFactory('foo',
              headers: {'content-type': ContentType.JSON.mimeType},
              body: '{"accountNumber": 10}'),
          '{"accountNumber":10}'),
      execution(
          'FORM content-type',
          createRequestFactory('foo',
              headers: {'content-type': ContentType.FORM.mimeType},
              body: 'accountNumber=10'),
          '{"accountNumber":10}')
    ]);

Account classTypeFromJsonBody(@RequestBody() Account account) => account;

HandlerCase classTypeFromJsonBodyCase = new HandlerCase(
    'A Handler with JSON dart class body and return',
    classTypeFromJsonBody,
    createRequestFactory('foo', body: '{"accountNumber": 10}'),
    '{"accountNumber":10}',
    [jsonClassBodyParam('account', Account)]);

Account classTypeFromFormBody(
        @RequestBody(format: ContentType.FORM) Account account) =>
    account;

Map mapTypeFromFormBody(@RequestBody(format: ContentType.FORM) Map arg1) =>
    arg1;

HandlerCase classTypeFromFormBodyCase = new HandlerCase(
    'A Handler with FORM dart class body and return',
    classTypeFromFormBody,
    createRequestFactory('foo', body: 'accountNumber=10'),
    '{"accountNumber":10}', [
  formClassBodyParam('account', Account, [
    new MemberBinding(
        #accountNumber, #accountNumber, new SimpleParameterTypeConverter(int))
  ])
]);

Account classTypeFromParams(@PathParams() Account account) => account;

HandlerCase classTypeFromParamsCase = new HandlerCase(
    'A Handler with dart class from params',
    classTypeFromParams,
    createRequestFactory('foo', pathParams: {"accountNumber": '10'}),
    '{"accountNumber":10}', [
  pathParams('account', Account, [
    new MemberBinding(
        #accountNumber, #accountNumber, new SimpleParameterTypeConverter(int))
  ])
]);

Account2 classTypeWithSettersFromParams(@PathParams() Account2 account) =>
    account;

HandlerCase classTypeWithSettersFromParamsCase = new HandlerCase(
    'A Handler with dart class from params using setters',
    classTypeWithSettersFromParams,
    createRequestFactory('foo', pathParams: {"accountNumber": '10'}),
    '{"accountNumber":10}', [
  pathParamsFromSetter('account', Account2, [
    new MemberBinding(
        #accountNumber, #accountNumber, new SimpleParameterTypeConverter(int))
  ])
]);

String customObject(OohLookAtMeImCustom custom) => custom.message;

HandlerCase customObjectCase = new HandlerCase(
    'A Handler with a custom object',
    customObject,
    createRequestFactory('foo'),
    '"custom thingy"',
    [new CustomObjectParameterBinding('custom', oohLookAtMeImCustomFactory)]);

String headerParameters(@HeaderParam(headerName: 'my-header') int myHeader,
        {@HeaderParam() String myOtherHeader: 'foo',
        @HeaderParam() String myOtherOtherHeader}) =>
    _returnVal([myHeader, myOtherHeader, myOtherOtherHeader]);

HandlerCase headerParametersCase = new HandlerCase(
    'A handler with header parameters',
    headerParameters,
    createRequestFactory('foo',
        headers: {'my-header': '5', 'my-other-other-header': 'yep'}),
    '"5-foo-yep"',
    [
      headerParam('myHeader', int, false, 'my-header'),
      headerParam('myOtherHeader', String, true, 'my-other-header'),
      headerParam('myOtherOtherHeader', String, true, 'my-other-other-header')
    ]);

typedef Request RequestFactory();

class HandlerCase<R> {
  final String description;
  final Function handlerFunction;
//  final Uri requestUri;
//  final RequestFactory requestFactory;
//  final R expectedReturn;

  final Iterable<ExecutionCase> executionCases;

  final Iterable<HandlerParameterBinding> parameterBindings;
  final ResponseBinding responseBinding;

  HandlerCase(this.description, this.handlerFunction,
      RequestFactory requestFactory, R expectedReturn, this.parameterBindings,
      [this.responseBinding,
      Iterable<ExecutionCase> additionalExecutionCases = const []])
      : this.executionCases = concat([
          [new ExecutionCase('sunny day', requestFactory, expectedReturn)],
          additionalExecutionCases
        ]);

  HandlerBinding get handlerBinding =>
      new HandlerBinding(parameterBindings, handlerFunction, responseBinding);
}

ExecutionCase execution(
        String description, RequestFactory requestFactory, expectedReturn) =>
    new ExecutionCase(description, requestFactory, expectedReturn);

class ExecutionCase<R> {
  final String description;
  final RequestFactory requestFactory;
  final R expectedReturn;

  ExecutionCase(this.description, this.requestFactory, this.expectedReturn);
}

HandlerParameterBinding pathParam(String handlerParamName, Type paramType,
        [bool isNamed = false, String pathParamName]) =>
    new BasicHandlerParameterBinding(
        handlerParamName,
        isNamed,
        new PathHandlerParameterSource(
            pathParamName != null ? pathParamName : handlerParamName),
        new SimpleParameterTypeConverter(paramType));

HandlerParameterBinding headerParam(String handlerParamName, Type paramType,
        [bool isNamed = false, String headerName]) =>
    new BasicHandlerParameterBinding(
        handlerParamName,
        isNamed,
        new HeaderHandlerParameterSource(
            headerName != null ? headerName : handlerParamName),
        new SimpleParameterTypeConverter(paramType));

HandlerParameterBinding simpleBodyParam(
        String handlerParamName, Type paramType) =>
    new BasicHandlerParameterBinding(
        handlerParamName,
        false,
        new BodyHandlerParameterSource(),
        new SimpleParameterTypeConverter(paramType));

HandlerParameterBinding contentTypeClassBodyParam(String handlerParamName,
        Type paramType, Iterable<MemberBinding> memberBindings) =>
    new BasicHandlerParameterBinding(
        handlerParamName,
        false,
        new BodyHandlerParameterSource(),
        new MapBasedParameterTypeConverter(
            requestContentTypeMapDecoder,
            new ContentTypeBasedObjectConverter({
              ContentType.JSON: new JsonToObjectConverter.fromType(paramType),
              ContentType.FORM: new ConstructorBasedMapToObjectConverter(
                  new TypeBinding.build(
                      type: paramType,
                      memberBindings: memberBindings,
                      bindMode: BindMode.CONSTRUCTOR,
                      constructorName: #build))
            }, new JsonToObjectConverter.fromType(paramType))));

HandlerParameterBinding jsonClassBodyParam(
        String handlerParamName, Type paramType) =>
    new BasicHandlerParameterBinding(
        handlerParamName,
        false,
        new BodyHandlerParameterSource(),
        new MapBasedParameterTypeConverter(
            jsonMapDecoder, new JsonToObjectConverter.fromType(paramType)));

HandlerParameterBinding formClassBodyParam(String handlerParamName,
        Type paramType, Iterable<MemberBinding> memberBindings) =>
    new BasicHandlerParameterBinding(
        handlerParamName,
        false,
        new BodyHandlerParameterSource(),
        new MapBasedParameterTypeConverter(
            formMapDecoder,
            new ConstructorBasedMapToObjectConverter(new TypeBinding.build(
                type: paramType,
                memberBindings: memberBindings,
                bindMode: BindMode.CONSTRUCTOR,
                constructorName: #build))));

HandlerParameterBinding formMapBodyParam(String handlerParamName) =>
    new BasicHandlerParameterBinding(
        handlerParamName,
        false,
        new BodyHandlerParameterSource(),
        new MapBasedParameterTypeConverter(
            formMapDecoder, new IdentityMapToObjectConverter()));

HandlerParameterBinding pathParams(String handlerParamName, Type paramType,
        Iterable<MemberBinding> memberBindings) =>
    new BasicHandlerParameterBinding(
        handlerParamName,
        false,
        new MultiPathHandlerParameterSource(),
        new MapBasedParameterTypeConverter(
            identityMapDecoder,
            new ConstructorBasedMapToObjectConverter(new TypeBinding.build(
                type: paramType,
                memberBindings: memberBindings,
                bindMode: BindMode.CONSTRUCTOR,
                constructorName: #build))));

HandlerParameterBinding pathParamsFromSetter(String handlerParamName,
        Type paramType, Iterable<MemberBinding> memberBindings) =>
    new BasicHandlerParameterBinding(
        handlerParamName,
        false,
        new MultiPathHandlerParameterSource(),
        new MapBasedParameterTypeConverter(
            identityMapDecoder,
            new SetterBasedMapToObjectConverter(new TypeBinding.build(
                type: paramType,
                memberBindings: memberBindings,
                bindMode: BindMode.PROPERTY,
                constructorName: #build))));

RequestFactory createRequestFactory(String requestUriPath,
    {Map pathParams: const {}, Map headers: const {}, body}) {
  final context = {};
  setPathParametersInContext(context, pathParams);
  return () => new Request(
      'GET', Uri.parse('http://example.com/$requestUriPath'),
      headers: headers, context: context, body: body);
}

class Account {
  final int accountNumber;

  Account(this.accountNumber);

  Account.build({this.accountNumber});

  Account.fromJson(Map json) : this(json['accountNumber']);

  Map toJson() => {'accountNumber': accountNumber};
}

class Account2 {
  int accountNumber;

  Map toJson() => {'accountNumber': accountNumber};
}

class OohLookAtMeImCustom {
  final String message = 'custom thingy';
}

OohLookAtMeImCustom oohLookAtMeImCustomFactory(Request request) =>
    new OohLookAtMeImCustom();
