## 0.9.3

- updates to dependencies

## 0.9.2

* fixed handling of dynamic parameters and returns
* fixed handling of FORM body params where the parameter type is Map

## 0.9.0

_Warning: There are several things in this release that are potentially  **BREAKING**_

Whether it is breaking for your case depends on how you use it. For most users I expect little to no impact (for example there was no impact in these changes for [backlog.io][backlog.io]).

This is a major rewrite of the internals. In addition to many new features, the default behaviour has changed to minimise the need for annotations. The most common cases should require no annotations.

The changes are:

* if you want to tweak the bindings you must now use the new annotations (such as `@PathParam`, `@PathParams`, `@BodyRequest` etc).
	* The old way of passing in your own bindings into the `bind` function is no longer supported.
	* You can alternatively use the new `bindManually` function but you must provide all the bindings in this case. No inference will be done if you call this function.
	    * Additionally the binding classes has changed substantially in this release
* the default behaviour for inferring the bindings between the request and the handler parameters has changed
    * handler parameters that are not simple types are now by default assumed to come from the body, not request path parameters.
	    * this includes `Map`, `List` and any of your classes (that are not registered as `custom objects`).
	    * If no annotation is added to the parameter then `shelf_bind` will first look at the request `content-type`
		    * if none is present it defaults to `JSON`
		    * if the `content-type` is set and is `FORM` or `JSON` then it will be handled as that type
		    * if it is any other `content-type` then a `400` response will be returned
   * If you map multiple path parameters into your own custom class you now need to use the `@PathParams` annotation
* there is now automatic conversion of snake case (e.g. `account_id`) in query parameters to camel case (e.g `accountId`) handler parameters
* you can now bind to header fields using the new `@HeaderParam` annotation
* there is automatic conversion of kebab case (e.g. `content-type`) in header fields to camel case (e.g. `contentType`) handler parameters


## 0.8.1

* unittest replaced with test
* explicit matcher dependency added, ported to 0.12.0 version
* expose more info to support shelf_rest style extensions

## 0.8.0

* Now uses http_exception package rather than shelf_exception_response

## 0.7.0+3

* increased upper bound on constrain dependency

## 0.7.0+2

* bumped self_exception_response dependency version

## 0.7.0+1

* increased upper bound on shelf_route dependency

## 0.7.0

* added support for mergeable handler adapters. This allows things like
customObjects to be extended by child routes

* removed deprecated methods

## 0.6.1+4

* widened shelf_route version range

## 0.6.1+3

* widened shelf_route version range

## 0.6.1+2

* minor doco changes

## 0.6.1+1

* Improved handling of some errors

## 0.6.1

* Improved integration with [constrain](https://pub.dartlang.org/packages/constrain) package
    * Support for constraints directly on parameters and returns
    * Constraint violations for all parameters included

## 0.6.0+1

* Fixed use of Deprecated

## 0.6.0

* New verson of shelf_route that fixed spelling mistake in HandlerAdapter

## 0.5.1

* Support for form encoded body

## 0.5.0

* **breaking change**: handlerAdapter is now a function
* Support for Custom parameter injection
* Integration with the [constrain](https://pub.dartlang.org/packages/constrain) package for automatic validation of
    * handler function parameters
    * handler function returns

## 0.4.2

* Added idField to ResponseHeaders annotation

## 0.4.1

* Added type converters for bool and DateTime

## 0.4.0
Major release with lots of changes including:

* Extensive binding support:
    * binding multiple function parameters,
    * simple type parameters,
    * custom classes (both to path parameters and body)
* Seamless integration with Shelf Route
* New annotations to customise bindings
* Simple type conversions
* Improved documentation
* Support for Shelf Path to remove direct dependency to Shelf Route

## 0.3.0

* Upgraded shelf and shelf route

## 0.2.2

* Made second argument (the request) to handler optional.

* Support for returning an object and automatically transforming to json

## 0.2.1

* Added support for binding to properties instead of to constructor args

## 0.2.0

* Upgrade shelf to 0.5.0 and shelf route to 0.4.0

## 0.1.0

* First version


[backlog.io]: http://backlog.io